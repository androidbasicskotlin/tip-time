package com.franck.tiptime

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.franck.tiptime.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    // We declare the view binding variable to be instantiate before to be use
    //with lateinit var
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Add click listener to the button calculate
        this.binding.calculateButton.setOnClickListener { calculateTip() }

        // Set a key listener on the TextInputEditText
        //binding.costOfServiceEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)  }
    }

    private fun calculateTip() {
        //Get the cost of service
        // We retrieve the value in the Edit Text
        val stringInTextField = binding.costOfServiceEditText.text.toString()
        // We parse that String value to Double
        val cost = stringInTextField.toDoubleOrNull()

        //verification
        if (cost == null || cost == 0.0){
            //clear the content of tip amount textView when the cost service text is empty
            //binding.tipResult.text = ""
            displayTip(0.0)

            return
        }

        //Get the tip percentage
        // We retrieve the selected id chosed among the RadioGroup buttons
        // val selectedId = binding.tipOptions.checkedRadioButtonId
        // Get the corresponding percentage of that selectedId
        val tipPercentage = when(binding.tipOptions.checkedRadioButtonId) {
            R.id.option_twenty_percent -> 0.20
            R.id.option_eighteen_percent -> 0.18
            else -> 0.15
        }

        /*
        Calculate the tip and round it up
        We calculate and save in the variable tip( var tip)
        since the value can change cause of rounding
        */
        var tip = tipPercentage * cost
        /*
        We need to check if the switch is "on" or "off"
        val roundUp = binding.roundUpSwitch.isChecked
        We round up if the switch is check
        */
        if (binding.roundUpSwitch.isChecked)  tip = kotlin.math.ceil(tip)

        //Format the tip
        //val formattedTip = NumberFormat.getCurrencyInstance().format(tip)
        // and display it
        //binding.tipResult.text = getString(R.string.tip_amount, formattedTip)
        displayTip(tip)
    }

    private fun displayTip(tip : Double) {
        val formattedTip = NumberFormat.getCurrencyInstance().format(tip)
        binding.tipResult.text = getString(R.string.tip_amount, formattedTip)
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}