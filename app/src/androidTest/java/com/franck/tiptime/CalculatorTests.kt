package com.franck.tiptime

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.Matchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CalculatorTests {

    @get:Rule
    val activity = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun calculate_20_percent_tip(){
        // We fill the edit text with a amount of $50.00
        onView(withId(R.id.cost_of_service_edit_text))
            .perform(typeText("50.00"))

        //close the keyboard if open
        // Because the keyboard sometimes hide the rest of the view (for small screen)
        Espresso.closeSoftKeyboard()

        // We click on the calculate button
        onView(withId(R.id.calculate_button))
            .perform(click())

        // We verify the result in the tip_result textView
        onView(withId(R.id.tip_result))
            .check(matches(withText("Tip Amount: $10.00")))

    }

    @Test
    fun calculate_15_percent_tip(){

        // We fill the edit text with a amount of $50.00
        onView(withId(R.id.cost_of_service_edit_text))
            .perform(typeText("50.00"))

        //close the keyboard if open
        // Because the keyboard sometimes hide the rest of the view (for small screen)
        Espresso.closeSoftKeyboard()

        // choose 15% tip option
        onView(withId(R.id.option_fifteen_percent))
            .perform(click())

        // We click on the calculate button
        onView(withId(R.id.calculate_button))
            .perform(click())

        // We verify the result in the tip_result textView
        onView(withId(R.id.tip_result))
            .check(matches(withText(containsString("$7.50"))))


    }

    @Test
    fun is_fifteen_checked(){

        onView(withId(R.id.option_fifteen_percent))
            .perform(click()).check(matches(isChecked()))
    }

}